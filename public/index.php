<?php

chdir(dirname(__DIR__.'../'));
require_once 'bootstrap.php';
require(\App::$baseDir.'/routesReques.php');

$klein = new \Klein\Klein();

array_map( function( $item ) use( $klein ){
	$klein->respond( $item['methods'], $item['key'], function( $request ) use( $item )  {

		$key = $item['key'];
		$controller = $item['controller'];
		$action = $item['action'];
		
		$obj = new $controller();
		$obj->$action( $request );

	});
}, \App::getRoutesList() );

$klein->dispatch();

\App::close();