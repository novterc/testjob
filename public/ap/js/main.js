var subUrlPrefix = location.pathname;
var urlGetFormEdit = subUrlPrefix+'/getFormEdit/';
var urlSetForm = subUrlPrefix+'/setForm';
var urlDelete = subUrlPrefix+'/delete/';
var urlchangeState = subUrlPrefix+'/changeStat/';
// alert('123');

function getFormEdit( id ){
	$.ajax({
		type: "POST",
		url: urlGetFormEdit+id,
		success: function( data ){
			$('#editPopUp .modal-body').html( data );
			setEventForm( $('#editPopUp .modal-body form') );
		}			
	});
}


function setForm(){
	console.log('setForm');
	// alert(123);
	$('#messageForm').ajaxForm({
		url: urlSetForm, 
		success: function( data ) { 
			location.reload();
		},
	}); 
}


function deleteItem( Id ){
	if( confirm("really delete?") ) {
		$.ajax({
			type: "GET",
			url: urlDelete+Id,			
		});
		location.reload();
	}
}


function changeState( Id, Stat ){
		$.ajax({
			type: "GET",
			url: urlchangeState+Id,	
			data: { stat: Stat },		
		});
		location.reload();
	
}


function setEventForm( form ){
	setForm();
}


$(document).ready(function(){

	$('#editPopUp').on('shown.bs.modal', function(e){
		var Id = $(e.relatedTarget).data('id');
		getFormEdit( Id );
	});

	$('#createPopUp').on('shown.bs.modal', function(e){
		getFormCreate();
	});

	$('.buttonDelete').on('click', function(e){
		var Id = $(e.target).data('id');
		deleteItem( Id );
	});

	$('.buttonShow').on('click', function(e){
		var Id = $(e.target).data('id');
		changeState( Id, 1 );
	});

	$('.buttonHide').on('click', function(e){
		var Id = $(e.target).data('id')
		changeState( Id, 0 );
	});

});
