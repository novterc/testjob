

function basicUploadFile( input, urlUploadImage, successCallBack )
{
	var file_data = $(input).prop('files')[0];
	var form_data = new FormData();
	form_data.append('images[]', file_data);
	$.ajax({
		url: urlUploadImage, // point to server-side PHP script 
		dataType: 'text',  // what to expect back from the PHP script, if anything
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'post',
		success: successCallBack,
	 });
}
	

$(document).ready(function(){

	$('.ajaxLoadModalBody').on('hide.bs.modal', function(){
		$('.ajaxLoadModalBody .modal-body').html( 'load...' );
	});

	$('.ajaxLoadModalBody .buttonAplayModal').on('click', function(){
		$(this).parents('.modal-content').find('.modal-body>form').submit();
	});

});