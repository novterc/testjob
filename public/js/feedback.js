var colorTime = setInterval( nextColor, 100 );
var color = [ 255, 0, 0 ];
var colorPart = 1;

function nextColor(){
	var step = 25;

	if( colorPart == 1 ){
		color[2] += step;
		if( color[2] >= 250 )
			colorPart = 2;
	}

	if( colorPart == 2 ){
		color[0] -= step;
		if( color[0] <= 0 )
			colorPart = 3;
	}

	if( colorPart == 3 ){
		color[1] += step;
		if( color[1] >= 250 )
			colorPart = 4;
	}

	if( colorPart == 4 ){
		color[2] -= step;
		if( color[2] <= 0 ){
			colorPart = 5;
		}
	}

	if( colorPart == 5 ){
		color[0] += step;
		if( color[0] >= 250 ){
			colorPart = 6;
		}
	}

	if( colorPart == 6 ){
		color[1] -= step;
		if( color[1] <= 0 ){
			colorPart = 1;
		}
	}

	$('body>.table-container').css('background-color','rgba( '+color[0]+', '+color[1]+', '+color[2]+', 0.5 )' );

}
