// play click
$('.player .play').click(function (e) {
	e.preventDefault();

	playAudio();
});

// pause click
$('.player .pause').click(function (e) {
	e.preventDefault();

	stopAudio();

	clearInterval( colorTime );
	$('.dance-animate').removeClass('dance-animate');
});


// forward click
$('.player .fwd').click(function (e) {
	e.preventDefault();

	stopAudio();

	var next = $('.playlist li.active').next();
	if (next.length == 0) {
		next = $('.playlist li:first-child');
	}
	initAudio( next );
	playAudio();
});

// rewind click
$('.player .rew').click(function (e) {
	e.preventDefault();

	stopAudio();

	var prev = $('.playlist li.active').prev();
	if (prev.length == 0) {
		prev = $('.playlist li:last-child');
	}
	initAudio( prev );
	playAudio();
});


// show playlist
$('.player .pl').click(function (e) {
	e.preventDefault();

	$('.playlist').toggle(300);
});

// playlist elements - click
$('.playlist li').click(function () {
	stopAudio();
	initAudio( $(this) );
	playAudio();
});


// inner variables
var song;
var tracker = $('.player .tracker');
var volume = $('.player .volume');


initAudio( $('.playlist li:first-child') );

song.volume = 0.8;

// initialize the volume slider
volume.slider({
	range: 'min',
	min: 1,
	max: 100,
	value: 80,
	start: function(event,ui) {},
	slide: function(event, ui) {
		song.volume = ui.value / 100;
	},
	stop: function(event,ui) {},
});

// empty tracker slider
tracker.slider({
	range: 'min',
	min: 0, max: 10,
	start: function( event,ui ) {},
	slide: function( event, ui ) {
		song.currentTime = ui.value;
	},
	stop: function(event,ui) {}
});

playAudio();


function initAudio( elem ) {
	var url = elem.attr('audiourl');
	var title = elem.text();
	// var cover = elem.attr('cover');
	var artist = elem.attr('artist');

	$('.player .title').text(title);
	$('.player .artist').text(artist);

	song = new Audio( url );

	song.addEventListener('timeupdate',function (){
		var curtime = parseInt( song.currentTime, 10);
		tracker.slider('value', curtime);
	});

	song.addEventListener('ended', function(){

		$('.player .fwd').click();
	} );

	$('.playlist li').removeClass('active');
	elem.addClass('active');

	song.oncanplay = function(){
		tracker.slider( "option", "max", song.duration );
	}
}


function playAudio() {
	song.play();

	$('.player .play').addClass('Plhidden');
	$('.player .pause').addClass('visible');
}


function stopAudio() {
	song.pause();

	$('.player .play').removeClass('Plhidden');
	$('.player .pause').removeClass('visible');
}
