angular.module( 'feedbackApp', [] )

.controller( 'FeedbackController', function( $scope, $http, orderByFilter, $sce ) {
	
		var feedback = this;
		feedback.messages = [];
		$scope.previewMsg = false;

		$http.get( '/getMsgs' )
			.success( function( data ){
			if ( !data.success ) {
				alert( 'ERROR' );
			} else {
				feedback.messages = data.msgs;
				$scope.sortBy( 'create' );
				$scope.sortBy( 'create' );
			}
		});

		$scope.preview = function( formData ) {
			if( $scope.previewMsg === false ){
				feedback.preview( formData );
			} else {
				feedback.previewClose();
			}
		}

		feedback.preview = function( formData ){
			$scope.previewMsg = feedback.messages.push( formData );
		}

		feedback.previewClose = function(){
			if( $scope.previewMsg !== false ){
				feedback.messages.splice( ( $scope.previewMsg - 1 ), 1 );
				$scope.previewMsg = false;
			}
		}

		feedback.getScopeAttr = function(scope){
			var data = {};
			for (var key in scope) {
				if( key.substr(0,1) != '$' && typeof scope[key] == "string" ){
					data[key] = scope[key];
				}
			}
			return data;
		};

		feedback.resetScopeAttr = function(scope){
			for (var key in scope) {
				if( key.substr(0,1) != '$' && typeof scope[key] == "string" ){
					scope[key] = '';
				}
			};
		};
		

		$scope.submit = function( form ) {
			var formData = feedback.getScopeAttr( form );
			feedback.resetScopeAttr( form )
			feedback.previewClose();
			$http({
					method: 'POST',
					url: '/addMsg',
					data: $.param( formData ),
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
				.success( function( data ) {
					if ( !data.success ) {
						alert( 'ERROR' );
					} else {
						feedback.messages.push( data.msg );
					}
				});
		}

		$scope.sortBy = function( propertyName ) {
			$scope.reverse = ( propertyName !== null && $scope.propertyName === propertyName ) ? !$scope.reverse : false;
			$scope.propertyName = propertyName;
			feedback.messages = orderByFilter( feedback.messages, $scope.propertyName, $scope.reverse);
		};

		$scope.renderHtml = function( html_code )
		{
		    return $sce.trustAsHtml( html_code );
		};

	})

.directive( "fileread", [ function () {
	return {
		scope: {
			fileread: "="
		},
		link: function ( scope, element, attributes ) {
			element.bind( "change", function ( changeEvent ) {
				var reader = new FileReader();
				reader.onload = function ( loadEvent ) {
					scope.$apply(function () {
						scope.fileread = loadEvent.target.result;
					});
				}
				reader.readAsDataURL( changeEvent.target.files[0] );
			});
		}
	}
}]);
