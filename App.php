<?php

class App
{
	protected static $_instance;
	protected static $_routes;
	protected static $_blade;
	protected $_configs;

	public static $baseDir;

	protected function __clone() {}
	protected function __construct() {}
	protected function __wakeup() {}


	public static function get()
	{
		if( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}


	public function init( $basicDir, $configsDir )
	{
		self::$baseDir = $basicDir;
		if( is_array( $configsDir ) ) {
			$configs = [];
			foreach ( $configsDir as $confDir ) {
				$conf = self::get()->loadConfigs( $confDir );
				$configs = array_replace_recursive( $configs, $conf );
			}
			self::get()->_configs = $configs;
		} else {
			self::get()->_configs = self::get()->loadConfigs( $configsDir );
		}
	}


	public static function close()
	{}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function getConf( $path )
	{
		$path_arr = explode( '.', $path );
		$result = null;
		$_configs = $this->_configs;
		foreach ( $path_arr as $key ){
			if( isset( $_configs[ $key ] ) ){
				$result = $_configs[ $key ];
				$_configs = $_configs[ $key ];
			}else{
				throw new Exception("Error undefined confug : \"{$path}\" ", 1001 );
			}
		}
		return $result;
	}


	public static function conf( $path )
	{
		return self::get()->getConf( $path );
	}


	public function loadConfigs( $configsDir )
	{
		$configs = [];
		$files = scandir( $configsDir );
		foreach ( $files as $file ) {
			if( array_search( $file, [ '.', '..' ] ) !== false  ){
				continue;
			}
			$name = basename( $file, '.php');
			$pathConfig = $configsDir.'/'.$name.'.php';
			$configs[ $name ] = include( $pathConfig );
		}
		return $configs;
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function getBlade()
	{
		if( self::$_blade === null ){
			$views = self::$baseDir . self::conf('main.template.views');
			$cache = self::$baseDir . self::conf('main.template.cache');
			$blade = new \Philo\Blade\Blade( $views, $cache );
			self::$_blade = $blade; 
		}

		return self::$_blade;
	}


	public static function render( $template, $values=[] )
	{
		echo self::getBlade()->view()->make( $template, $values )->render();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function addRoute( $methods, $key, $controller, $action, $autoCache=false, $exp=false, $appapi=false )
	{
		self::$_routes[] = [
			'methods' => $methods,
			'key' => \App::conf('main.public.prefix').$key,
			'controller' => $controller,
			'action' => $action,
			'autoCache' => $autoCache,
			'exp' => $exp,
			'appapi' => $appapi,
		];
	} 


	public static function getRoutesList()
	{
		return self::$_routes;
	}


	public static function getLink( $controller, $action, $argv = [], $number = 0 )
	{	
		$selected = [];
		foreach ( self::$_routes as $route) {
			if( $route['controller'] === $controller && $route['action'] === $action ){
				$selected[] = $route;
			}
		}

		if( !isset( $selected[ $number ] ) )
			return null;

		$routeSelect = $selected[ $number ];
		return self::conf( 'main.public.prefix' ) . self::vsprintf_named_new( $routeSelect['key'], $argv );
	}


	public static function vsprintf_named_new( $format, $args ) 
	{
		$names = preg_match_all( '/\[(.*?)\]/', $format, $matches, PREG_SET_ORDER );

		$values = array();
		foreach( $matches as $key => $match ) {
			$item = explode( ':', $match[1] );
			if( isset( $args[ $item[1] ] ) )
				$values[] = $args[ $item[1] ];
			else
				$values[] = '';

			$format = preg_replace( '/\[(.*?)\]/', '%s', $format, ( $key+1 ) );
		}

		return vsprintf( $format, $values );
	}


}
