<?php
 
error_reporting( E_ALL );
ini_set( "display_errors", 1 );
// ini_set( 'html_errors', 0 );
ini_set( "memory_limit", "258M" );
define( 'DEV_MODE', true );

require_once 'vendor/autoload.php';

\App::get()->init( 
	__DIR__, 
	[ 
		__DIR__.'/configs/production', 
		// __DIR__.'/configs/dev-novterc',
	] 
);
