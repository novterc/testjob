<?php

namespace Controllers;
use \Models\Msg;

Class Feedback extends Basic
{
	

	public function __construct()
	{
		mb_internal_encoding( 'UTF-8' );
		session_start();
	}


	public function main()
	{
		$this->_render( 'public.pages.feedback' );
	}


	public function getMsgs()
	{
		$msgs = Msg::instanceAllByStatAndSession( 1, session_id() );
		$msgsData = array_map( function( $msg ){ 
			return $msg->getDefaultData();
		}, $msgs );
		
		$resultArray = [
			'success' => true,
			'msgs' => $msgsData,
		];
		echo $this->_toJSON( $resultArray );
	}


	public function addMsg()
	{
		if( empty( $_POST[ 'name' ] ) || empty( $_POST[ 'email' ] ) || empty( $_POST[ 'text' ] ) ){
			echo '0';
			return false;
		}

		$msg = Msg::create();

		if( !empty( $_POST['image'] ) ){
			$imagePath = $msg->uploadImage( $_POST['image'] );
		} 

		$msg->create_unixtime = time();
		$msg->state = 0;
		$msg->session = session_id();
		$msg->name = htmlspecialchars( $_POST['name'] );
		$msg->email = htmlspecialchars( $_POST['email'] );
		$msg->text = htmlspecialchars( $_POST['text'] );
		$msg->save();

		$resultArray = [
			'success' => true,
			'msg' => $msg->getDefaultData(),
		];
		echo $this->_toJSON( $resultArray );
	}

	
}
