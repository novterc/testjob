<?php

namespace Controllers;
use \Models\User;
use \Models\Msg;

Class Ap extends Basic
{
	

	public function __construct()
	{
		mb_internal_encoding("UTF-8");
		session_start();

		$this->auth();
	}


	public function auth()
	{
		if( empty( $_SESSION['userId'] ) ) {

			if( isset( $_POST['inputEmail'], $_POST['inputPassword'] ) ) {
				$user = User::instanceByEmailPass( $_POST['inputEmail'], $_POST['inputPassword'] );
				if( $user !== false ){
					$this->_setAuthAdmin( $user );
					$this->_setAuthlog( true );
					return true;
				} else {
					$this->_setAuthlog( false );
				}
			} elseif ( isset( $_GET['inviteToken'] ) && \App::checkToken( $_GET['inviteToken'] ) ){
				$user = User::instanceById( 1 );
				$this->_setAuthAdmin( $user );
				$this->_setAuthlog( true );
				return true;
			} 
		   
		} else {
			$user = User::instanceById( $_SESSION['userId'] );
			if( $user !== false ){
				$this->_setAuthAdmin( $user );
				return true;
			}
		}
	 
		\App::render('admin.pages.auth');
		die;
	}


	protected function _setAuthAdmin( $user )
	{
		$this->_user = $user;
		$_SESSION['userId'] = $user->id;			
	}


	protected function _setAuthlog( $status )
	{
		$requestDataArr = $_REQUEST;
		$requestDataArr['inputPassword'] = '********';
		$requestData = serialize( $requestDataArr );
	}


	public function main()
	{
		$msgs = Msg::instanceAllDescId();

		\App::render( 'admin.pages.main', [
			'msgs' => $msgs,
		] );
	}

	
	public function logout()
	{
		session_destroy();
		header('Location: '.\App::getLink( '\Controllers\Feedback', 'main'));
		die;
	}


	public function getFormEdit( $request )
	{
		$msg = Msg::instanceById( $request->id );
		// var_dump( $msg ); die;
		$this->_render( 'admin.sections.messageForm', [
			'msg' => $msg,
		]);
	}


	public function setForm( $request )
	{
		$msg = Msg::instanceById( $request->id );
		if( $msg == false ){
			echo '1901';
			return false;
		}

		$msg->name = htmlspecialchars( $_POST['name'] );
		$msg->email = htmlspecialchars( $_POST['email'] );
		$msg->text = htmlspecialchars( $_POST['text'] );

		$msg->state = (int) @$_POST['state'];
		$msg->admin_edit = 1;
		$msg->save();

		echo '1';
	}


	public function delete( $request )
	{
		$msg = Msg::instanceById( $request->id );
		if( $msg == false ){
			echo '1902';
			return false;
		}

		$msg->delete();

		echo '1';
	}


	public function changeStat( $request )
	{
		$msg = Msg::instanceById( $request->id );
		if( $msg == false ){
			echo '1902';
			return false;
		}

		$msg->state = intval( $_GET['stat'] );
		$msg->save();

		echo '1';
	}
	

}
