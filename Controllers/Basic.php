<?php

namespace Controllers;

class Basic
{


	public function __construct()
	{}


	protected function _toJSON( $arr )
	{
		return json_encode( $arr, JSON_UNESCAPED_UNICODE );
	}


	protected function _render( $template, $values = [] )
	{
		\App::render( $template, $values );
	}


	public static function redirect( $url )
	{
		header('Location: '.$url);
		die;
	}


	public static function viewToJSON( $arr )
	{
		if( isset($_GET['dev']) ){
			echo '<pre>'; 
			print_R( $arr ); 
			echo '</pre>';
		} else {
			echo self::toJSON( $arr );
		}
	}


}
