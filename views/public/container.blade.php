@extends('public.basic')
@section('body')

	<div class="table-container container-fluid">
		<div class="row">
			<div class="col-md-4 no-float dance-animate"></div><div class="col-md-9">@yield('container')</div><div class="col-md-4 no-float dance-animate"></div>
		</div>
	</div>

@endsection
