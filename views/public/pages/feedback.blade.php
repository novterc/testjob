@extends('public.container')

@section('title')@parent.FEEDBACK @endsection

@section('header')
	@parent
	<link href="/css/feedback.css" rel="stylesheet">
@endsection

@section( 'bottomScripts')
	@parent
	<script type="text/javascript" src="/js/angular.min.js"></script>
	<script type="text/javascript" src="/js/feedbackController.js"></script>
	<script type="text/javascript" src="/js/feedback.js"></script>
@endsection

@section('container')
	
	@include('public.sections.player')

	<div class="page-header">
		<marquee>
			<h1>DISCO...DISCO...FEEDBACK </h1>
		</marquee>
	</div>

	<div ng-app="feedbackApp" ng-controller="FeedbackController as feedback">

		<div class="sortesPanel my-block" >
			</span>Сортировка:</span>
			<button class="btn btn-default" ng-click="sortBy('name')" ng-class="{ 'sort': propertyName == 'name' , reverse: reverse}">Имя</button>
			<button class="btn btn-default" ng-click="sortBy('email')" ng-class="{ 'sort': propertyName == 'email' , reverse: reverse}">Email</button>
			<button class="btn btn-default" ng-click="sortBy('create')" ng-class="{ 'sort': propertyName == 'create' , reverse: reverse}">Дата</button>
		</div>
		<div class="list-group list-feedbacks">
			
			<div class="list-group-item my-block" ng-repeat="msg in feedback.messages">
				<div class="media">
					<div class="media-left">
						<img ng-src="{{ msg.image != null ? msg.image : '/img/default-avatar.png' }}" class="media-object my-thumbs" alt="img">
					</div>
					<div class="media-body" >
						<div class="media-heading"><strong><span ng-bind-html="renderHtml(msg.name)"></span> - <span ng-bind-html="renderHtml(msg.email)"></span></strong> {{msg.create}} </div>
						<span ng-bind-html="renderHtml(msg.text)"></span> 
						<div ng-show="msg.admin_edit == 1" class="admin-edit-msg" >
							изменен администратором
						</div>
					</div>
				</div>
			</div>
				
		</div>
	
		<form class="my-block" name="messageForm" ng-submit="submit( formData )">
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" class="form-control" name="name" ng-model="formData.name" required>
			</div>
			<div class="form-group">
				<label for="email">Email address:</label>
				<input type="email" class="form-control" name="email" ng-model="formData.email" required>
			</div>
			<div class="form-group">
				<label for="pwd">Images:</label>
				<input type="file" name="file" fileread="formData.image" class="form-control" accept="image/x-png, image/gif, image/jpeg">
			</div>
			<div class="form-group">
				<label for="pwd">Text:</label>
				<textarea name="text" ng-model="formData.text" class="form-control" required></textarea>
			</div>
			<button type="button" class="btn btn-default" ng-disabled="messageForm.$invalid" ng-click="preview( formData )">{{ previewMsg == false ? 'Предварительный просмотр' : 'Закрыть предварительный просмотр'}}</button>
			<button type="submit" class="btn btn-default" ng-disabled="messageForm.$invalid">Отправить</button>
		</form>

	</div>	
	
@endsection
