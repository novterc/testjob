@extends( 'basic' )
@section( 'title', 'DISCO...DISCO...' )
@section( 'header' )
  @parent

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">  
	
@endsection
@section( 'bottomScripts')
	@parent

	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.js"></script>
	<script type="text/javascript" src="/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/jquery.form.min.js"></script>

@endsection
