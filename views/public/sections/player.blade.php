
@section('header')
	@parent
	<link href="/css/player.css" rel="stylesheet">
@endsection

@section( 'bottomScripts')
	@parent
	<script type="text/javascript" src="/js/player.js"></script>
@endsection

<div class="player my-block">
	<div class="info">
		<div class="artist"></div>
		<div class="title"></div>
	</div>
	<div class="tracker my-slider"></div>
	<div class="volume my-slider"></div>
	<div class="pl"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></div>
	<div class="controls">
		<div class="rew"><span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span></div>

		<div class="play"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></div>
		<div class="pause"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></div>

		<div class="fwd"><span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span></div>
	</div>
</div>
<ul class="playlist Plhidden my-block">
	<li audiourl="/mp3/Michael Jackson – Billie jean.mp3" cover="cover.jpg" >Michael Jackson - Billie jean</li>
	<li audiourl="/mp3/Queen – I Want To Break Free.mp3" cover="cover.jpg" >Queen - I Want To Break Free</li>
	<li audiourl="/mp3/Paradisio - Bailando.mp3" cover="cover.jpg" >Paradisio - Bailando</li>
	<li audiourl="/mp3/Masterboy – Sweet Dream's.mp3" cover="cover.jpg" >Masterboy - Sweet Dream's</li>
	<li audiourl="/mp3/Pet Shop Boys – It's A Sin.mp3" cover="cover.jpg" >Pet Shop Boys - It's A Sin</li>
	<li audiourl="/mp3/Masterboy – Love Message.mp3" cover="cover.jpg" >Masterboy - Love Message</li>
	<li audiourl="/mp3/ABBA - Give me! Give me! Give me!.mp3" cover="cover.jpg" >ABBA - Give me! Give me! Give me!</li>
	<li audiourl="/mp3/La Bouche – S.O.S..mp3" cover="cover.jpg" >La Bouche - S.O.S.</li>
	<li audiourl="/mp3/AQUA – Dum di da di da.mp3" cover="cover.jpg" >AQUA - Dum di da di da</li>
</ul>
