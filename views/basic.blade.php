<!DOCTYPE html>
<html lang="eng">
	<head>
		<title>@yield('title', '')</title>

		@yield('header')

	</head>
	<body>
	
		@yield('body')
		@yield('bottomScripts')
	
	</body>
</html>