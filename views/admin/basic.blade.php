@extends('basic')
@section('title', 'Ap')
@section('header')
  @parent

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="<{\App::conf('main.public.prefix')}>/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<{\App::conf('main.public.prefixAdmin')}>/css/admin.css" rel="stylesheet">

	<script src="/js/jquery.min.js"></script>
	<script src="/js/jquery.form.min.js"></script>
	<script src="<{\App::conf('main.public.prefixAdmin')}>/js/basic.js"></script>
	<script src="<{\App::conf('main.public.prefix')}>/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
	
	
@endsection
