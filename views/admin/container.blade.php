@extends('admin.basic')
@section('header')
	@parent
	
@endsection
@section('body')

	@include('admin.sections.navbar')
	
	<div class="container">
		@yield('container')
	</div>

@endsection
