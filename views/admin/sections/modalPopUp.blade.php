<!-- modalPopUp -->
  <div id="<{$id}>" class="modal fade ajaxLoadModalBody" role="dialog">
	<div class="modal-dialog">

	  <!-- Modal content-->
	  <div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title"><{$title}></h4>
		</div>
		<div class="modal-body">
		  <{$body}>
		</div>
		<div class="modal-footer">
		  <div>
			<!--<div class="alert alert-info">
			  <strong>Info!</strong> Indicates a neutral informative change or action.
			</div>-->
		  </div>
		  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  <button type="button" class="buttonAplayModal btn btn-primary" >Apply</button>
		</div>
	  </div>

	</div>
  </div>