<form id="messageForm" method="POST" action="" class="form-horizontal" role="form">
		<input type="hidden" name="id" value="<{isset($msg)?$msg->id:0}>" />
		
		<div class="form-group">
			<label class="control-label col-sm-2" for="name">Name:</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="name" name="name" value="<{@$msg->name}>" placeholder="Example">
			</div>
		</div> 

		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Email:</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="email" name="email" value="<{@$msg->email}>" placeholder="Example">
			</div>
		</div> 

		<div class="form-group">
			<label class="control-label col-sm-2" for="text">Text:</label>
			<div class="col-sm-10">
				<textarea class="form-control" name="text"><{@$msg->text}></textarea>  
			</div>
		</div> 

		<div class="form-group">
			<label class="control-label col-sm-2" for="state">State: </label>
			<div class="col-sm-10"> 
				<select class="form-control" id="state" name="state">
					
						<option value="0" <{ ( isset( $msg ) && $msg->state == 0 ) ? 'selected=selected' : '' }>>OFF</option>
						<option value="1" <{ ( isset( $msg ) && $msg->state == 1 ) ? 'selected=selected' : '' }>>ON</option>
			 
				</select>
			</div>
		</div>

</form>

