@extends('admin.container')
@section('title')@parent-auth @endsection
@section('header')
	@parent
	<link rel="stylesheet" href="<{\App::conf('main.public.prefixAdmin')}>/css/auth.css">
@endsection
@section('body')

	<div class="container">
		<form class="form-signin" method="POST">
			<h2 class="form-signin-heading">Please log in</h2>
			<label for="inputEmail" class="sr-only">Email address</label>
			<input type="text" id="inputEmail" name="inputEmail" class="form-control" placeholder="login" required autofocus>
			<label for="inputPassword" class="sr-only">Password</label>
			<input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required>
			
			<button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
		</form>
	</div>

@endsection