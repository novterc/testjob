@extends('admin.container')
@section('title')@parent-main @endsection
@section('header')
	@parent
	<script src="<{\App::conf('main.public.prefixAdmin')}>/js/main.js"></script>
@endsection
@section('container')

	<div class="top-container">
		<h2>Messages</h2> 
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Name</th>
				<th>Email</th>
				<th>Text</th>
				<th>Img</th>
				<th>State</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
				@foreach ( $msgs as $msg )
				<tr>
					<td><{ $msg->id }></td>
					<td><{ date( 'Y.m.d H:i:s', $msg->create_unixtime ) }></td>
					<td><{ $msg->name }></td>			
					<td><{ $msg->email }></td>			
					<td><{ $msg->text }></td>	
					<td><img class="table-thump" src="<{\App::conf( 'main.public.userImage' ) .'/'. $msg->image}>" /></td>
						<td><{($msg->state == '1'?'принят':'отклонен')}></td> 		
					<td>
						<div class="my-buttons-content">
							
							@if( $msg->state == '1' )
							<button type="button" class="btn btn-primary btn-xs buttonHide" data-id="<{$msg->id}>" >Hide</button>
							@else
							<button type="button" class="btn btn-success btn-xs buttonShow" data-id="<{$msg->id}>" >Show</button>
							@endif

							<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#editPopUp" data-id="<{$msg->id}>" >Edit</button>
							<button type="button" class="btn btn-danger btn-xs buttonDelete" data-id="<{$msg->id}>" >Delete</button>

						</div>
					</td>
				</tr>	 
				@endforeach
		</tbody>
	</table>

	@include('admin.sections.modalPopUp',[ 
	  'id' => 'editPopUp',
	  'title' => 'Message edit',
	  'body' => 'Load...',
	])

@endsection
