<?php

\App::addRoute( 'GET',			'/', 							'\Controllers\Feedback', 'main' );
\App::addRoute( 'POST',			'/addMsg', 						'\Controllers\Feedback', 'addMsg' );
\App::addRoute( 'GET',			'/getMsgs', 					'\Controllers\Feedback', 'getMsgs' );

$apPref = '/admin';

\App::addRoute( ['GET','POST'],	$apPref, 							'\Controllers\Ap', 	'main' );
\App::addRoute( 'GET',			$apPref.'/logout', 					'\Controllers\Ap', 	'logout' );

\App::addRoute(['GET','POST'], $apPref.'/getFormEdit/[i:id]',		'\Controllers\Ap'	,'getFormEdit');
\App::addRoute(['GET','POST'], $apPref.'/setForm',					'\Controllers\Ap'	,'setForm');
\App::addRoute(['GET','POST'], $apPref.'/delete/[i:id]',			'\Controllers\Ap'	,'delete');
\App::addRoute(['GET','POST'], $apPref.'/changeStat/[i:id]',		'\Controllers\Ap'	,'changeStat');