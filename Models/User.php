<?php

namespace Models;

Class User extends Model
{
	protected static $_tableName = 'users';
	protected static $_fields = [ 
		'id',  
		'email',
		'password_hash',
	];


	public static function create()
	{
		return static::_create();
	}


	public function load( $data )
	{
		return static::_load( $data );
	}


	public function save()
	{
		return $this->_save();
	}


	public function delete()
	{
		return $this->_delete();
	}


	public static function getPassHash( $pass )
	{
		return md5( 'DSds4GAD*&#YDUSHFSFds4DF$#' . $pass . 'DJ$%Ff3dsHSdYF&dhfshf73ysd' );
	}


	public static function instanceByEmailPass( $email, $pass )
	{
		$passHash = self::getPassHash( $pass ); 
		$data = static::_getDataByWhere( [ [ 'email', '=', $email ], [ 'password_hash', '=', $passHash ] ] );
		if( !is_array( $data ) ){
			return false;
		}
		$user = new static( $data );
		return $user;
	}
	

	public function setPassword( $password )
	{
		$hash = self::getPassHash( $password );
		$this->password_hash = $hash;
	}
	

}
