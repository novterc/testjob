<?php 

namespace Models\Data;

Class Mysql
{
	protected static $_mysqlLink;

	private function __clone() {}
	private function __construct() {}
	private function __wakeup() {}


	public static function get( $config = false )
	{
		if( $config === false ){
			$config = \App::conf( 'data.mysql' );
		}

		$confKey = serialize( $config );
		if( empty( self::$_mysqlLink[ $confKey ] ) ){
			$link = mysqli_connect( 
						$config['localhost'],
						$config['user'],
						$config['password'],
						$config['basename']
			);

			if ( !$link ) { 
				$mysqlError = mysqli_connect_error();
				throw new Exception("Error connected to database : \"{$mysqlError}\" ", 1101 );
				exit; 
			} 

			self::$_mysqlLink[ $confKey ] = $link;		
		}
		
		return self::$_mysqlLink[ $confKey ];
	}


	public static function query( $query, $config = false )
	{
		$link = self::get( $config );
		$result = mysqli_query( $link, $query );
		if( $result === false && DEV_MODE === true ){
			echo "\r\n<br/>-------------------------------------------------------<br/>\r\n";
			printf(" Ошибка: %s\n", mysqli_error( $link ) );
			echo "\r\n<br/>\r\n";
			echo $query;
			echo "\r\n<br/>-------------------------------------------------------<br/>\r\n";
		}
		return $result; 
	}


	public static function getItemQuery( $query, $config = false )
	{
		$result = static::query( $query, $config );
		$item = $result->fetch_assoc();
		return $item;
	}


	public static function getArrayQuery( $query, $config = false )
	{
		$result = static::query( $query, $config );
		$arrayResult = [];
		while( $mres = $result->fetch_assoc() ){
			$arrayResult[] = $mres;
		}
		return $arrayResult;
	}


	public static function getLastId( $config = false )
	{
		$link = self::get( $config );
		return mysqli_insert_id( $link );
	}


	public static function esc( $str, $config = false )
	{
		$link = self::get( $config );
		return mysqli_escape_string( $link, $str );
	}


	public static function whereArrToStr( $wheresArr ) {
		$whereItemsArr = [];
		foreach ( $wheresArr as $whereItem ) {
			$whereItem[2] = Mysql::esc( $whereItem[2] );
			$whereItemsArr[] = "`{$whereItem[0]}` {$whereItem[1]} '{$whereItem[2]}'"; 
		} 
		$wheresStr = implode(' AND ', $whereItemsArr );
		return $wheresStr;
	}


}
