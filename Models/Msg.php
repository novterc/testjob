<?php

namespace Models;
use \Models\Data\Mysql;

Class Msg extends Model
{
	protected static $_tableName = 'messages';
	protected static $_fields = [ 
		'id',  
		'create_unixtime',
		'state',
		'session',
		'name',
		'email',
		'text',
		'image',
		'admin_edit',
	];


	public static function create()
	{
		return static::_create();
	}


	public function load( $data )
	{
		return static::_load( $data );
	}


	public function save()
	{
		return $this->_save();
	}


	public function delete()
	{
		return $this->_delete();
	}


	public function getDefaultData()
	{
		if( empty( ($this->image) ) === false )
			$image = \App::conf( 'main.public.userImage' ) . '/' . $this->image;
		else
			$image = null;

		return [
			'id' => $this->id,
			'create' => date( 'Y.m.d H:i', $this->create_unixtime ),
			'state' => $this->state,
			'name' => $this->name,
			'email' => $this->email,
			'text' => $this->text,
			'image' => $image,
			'admin_edit' => $this->admin_edit,
		];
	}


	public static function instanceAllByStatAndSession( $stat, $session )
	{
		$tableName = static::_getTableName();
		$dataArr = Mysql::getArrayQuery(" SELECT * FROM `{$tableName}` WHERE `state` = '{$stat}' OR `session` = '{$session}' ORDER by `id` ");
		$objsArr = static::_instanceByArr( $dataArr );

		return $objsArr;
	}


	public function uploadImage( $imageData )
	{
		$pathTmpFile = $this->_saveEncodeImage( $imageData );
		if( $pathTmpFile === false  ){
			return false;
		}
	
		$checkRes = $this->_checkImage( $pathTmpFile );
		if( $checkRes === false ){
			unlink( $pathTmpFile );
			return false;
		} 
   
		$imagePath = $this->_resizeImage( $pathTmpFile );
		if( $imagePath === false ){
			unlink( $pathTmpFile );
			return false;
		}

		unlink( $pathTmpFile );

		$this->image = basename( $imagePath );
		$this->save();

		return true;
	}


	protected function _saveEncodeImage( $imageData )
	{
		$ss = strpos( $imageData, ':' );
		$sf = strpos( $imageData, ';' );
		$mime = substr( $imageData, ( $ss + 1 ), ( $sf - $ss - 1 ) );
		$h = ';base64,';
		$dataStart = strpos( $imageData, $h );
		$dataStart += strlen( $h );
		$data = substr_replace( $imageData, '', 0, $dataStart );
		$data = base64_decode( $data );
		
		$fileName = $this->_getNameImage( $mime );
		$pathTmpFile = \App::$baseDir . \App::conf( 'main.paths.tmpUploadFile' ) . '/' . $fileName;
		$res = file_put_contents( $pathTmpFile, $data );
		if( $res === false )
			return false;

		return $pathTmpFile;
	}


	protected function _checkImage( $pathTmpFile )
	{
		$accessTypes = \App::conf( 'main.userImg.accessTypes' );

		if( is_file( $pathTmpFile ) === false ) 
			return false;

		$type = mime_content_type( $pathTmpFile );
		if( array_search( $type, $accessTypes ) === false )
			return false;

		$size = getimagesize( $pathTmpFile ); 
		if( $size === false )
			return false;

		return true;
	}


	protected function _resizeImage( $pathTmpFile )
	{
		$imageMime = strtolower( image_type_to_mime_type( exif_imagetype( $pathTmpFile ) ) );
		$fileName = $this->_getNameImage( $imageMime );		
		$savePath = \App::$baseDir . \App::conf( 'main.paths.userImage' ) . '/' . $fileName;

		list( $width, $height, $type, $attr ) = getimagesize( $pathTmpFile );

		if ( $width > \App::conf( 'main.userImg.maxSize.x' ) || $height > \App::conf( 'main.userImg.maxSize.y' ) ) {
			require_once \App::$baseDir . '/libs/php_image_magician.php';

			$magicianObj = new \imageLib( $pathTmpFile );	
			$magicianObj->resizeImage( \App::conf( 'main.userImg.maxSize.x' ), \App::conf( 'main.userImg.maxSize.y' ), 'landscape');
			$magicianObj->saveImage( $savePath, \App::conf( 'main.userImg.quality' ) );
		} else {
			copy( $pathTmpFile, $savePath );
		}

		return $savePath;
	}


	protected static function _getNameImage( $imageMime )
	{
		$ext = explode( "/", strtolower( $imageMime ) );
		$ext = '.' . strtolower( end( $ext ) );
		$fileName = rand( 10000, 990000 ).'_'.time().$ext;
		return $fileName;
	}


}
