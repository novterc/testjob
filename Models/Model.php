<?php 

namespace Models;

use \Models\Data\Mysql;

Class Model
{
	protected static $_fields = [];
	protected $_fieldsData = [];
	protected $test2 = 'a';
	protected $_setedFields = [];
	protected static $_convertLoad = [];
	protected static $_convertSave = [];


	public function __construct( $data )
	{
		$this->load( $data );
	}


	public function __get( $name )
	{
		if( isset( $this->_fieldsData[ $name ] ) )
			return $this->_fieldsData[ $name ];
		else
			return null;
	}


	public function __set( $name, $value )
	{
		if( array_search( $name, static::$_fields ) !== false ) {
			$this->_setedFields[ $name ] = true;
			$this->_fieldsData[ $name ] = $value;
			return true;
		}else{
			$this->$name = $value;
		} 		
	}


	public static function instanceById( $id )
	{
		$data = static::_getDataById( $id );
		if( $data === null ) 
			return false;
		$obj = new static( $data );
		return $obj; 
	}


	public static function instanceAll()
	{
		$dataArr = static::_getDataAll();
		$objsArr = static::_instanceByArr( $dataArr );
		return $objsArr; 
	}


	public static function instanceByWhere( $where )
	{
		$data = static::_getDataByWhere( $where );
		if( !is_array( $data ) ){
			return false;
		}
		$obj = new static( $data );
		return $obj;  
	}


	public static function instanceAllByWhere( $where, $order = false )
	{
		$arrayData = static::_getDataArrByWhere( $where, $order );
		if( !is_array( $arrayData ) ){
			return false;
		}
		$objsArr = static::_instanceByArr( $arrayData );
		return $objsArr;
	}


	public static function instanceAllDescId()
	{
		$tableName = static::_getTableName();
		$dataArr = Mysql::getArrayQuery(" SELECT * FROM `{$tableName}` ORDER by `id` DESC LIMIT 1000 ");
		$objsArr = static::_instanceByArr( $dataArr );
		return $objsArr; 
	}


	protected static function _instanceByArr( $dataArr )
	{
		$objsArr = [];
		if( is_array( $dataArr ) ){
			foreach ( $dataArr as $data ) {
				$objsArr[] = new static( $data );
			}
		}
		return $objsArr; 
	}


	protected function _load( $data )
	{
		$this->setFieldsData( $data, false );
		return true;
	}


	protected static function _create()
	{
		$tableName = static::_getTableName();
		$result = Mysql::query(" INSERT INTO `{$tableName}` () VALUES () ");
		if( $result === false )
			return false;
		$newId = Mysql::getLastId();
		$obj = static::instanceById( $newId );

		return $obj;

	}


	protected function _save( $dataArr = [] )
	{
		$data = array_merge( $this->getSetedFieldsData(), $dataArr );
		return static::_setDataById( $this->id, $data );
	}


	protected function _delete()
	{
		return static::_deleteById( $this->id );
	}


	protected static function _getTableName()
	{
		return static::$_tableName;
	}


	protected static function _getDataById( $id )
	{
		$tableName = static::_getTableName();
		$id = intval( $id );
		$data = Mysql::getItemQuery(" SELECT * FROM `{$tableName}` WHERE `id`='{$id}' ");
		return $data;
	}


	protected static function _getDataAll()
	{
		$tableName = static::_getTableName();
		$dataArr = Mysql::getArrayQuery(" SELECT * FROM `{$tableName}` ");
		return $dataArr;
	}


	public static function _getDataByWhere( $wheresArr )
	{
		$wheresStr = Mysql::whereArrToStr( $wheresArr );
		$tableName = static::_getTableName();
		$sql = " SELECT * FROM `{$tableName}` WHERE {$wheresStr} ";
		$data = Mysql::getItemQuery( $sql );
		return $data;
	}


	public static function _getDataArrByWhere( $wheresArr, $order = false )
	{
		$wheresStr = Mysql::whereArrToStr( $wheresArr );
		$tableName = static::_getTableName();
		$dataArr = Mysql::getArrayQuery(" SELECT * FROM `{$tableName}` WHERE {$wheresStr} {$order} ");
		return $dataArr;
	}


	public static function truncate()
	{
		$tableName = static::_getTableName();
		$res = Mysql::query(" TRUNCATE `{$tableName}` ");
		return $res;
	}


	protected static function _setDataById( $id, $data )
	{
		$tableName = static::_getTableName();
		$fields = static::$_fields;
		$uploadFieldsArr = [];
		foreach( $fields as $field ){
			if( isset( $data[ $field ] ) ){
				$itemData = Mysql::esc( $data[ $field ] );
				$uploadFieldsArr[] = "`{$field}`='{$itemData}'";
			}
		}
		
		if( empty( $uploadFieldsArr ) )
			return false; 

		$uploadFieldsStr = implode( ', ', $uploadFieldsArr );
		$id = intval( $id );
		$result = Mysql::query(" UPDATE `{$tableName}` SET {$uploadFieldsStr} WHERE `id` = '{$id}' ");
		return $result;
	}


	protected static function _deleteById( $id )
	{
		$tableName = static::_getTableName();
		$id = intval( $id );
		$sql = " DELETE FROM `{$tableName}` WHERE `id` = '{$id}' ";
		$result = Mysql::query( $sql );
		return $result;
	}


	public function addConvertSeter( $fieldName, $convertMethod )
	{
		static::$_convertLoad[ $fieldName ] = $convertMethod;
	}


	public function addConvertGeter( $fieldName, $convertMethod )
	{
		static::$_convertSave[ $fieldName ] = $convertMethod;
	}


	public function setFieldsData( $data, $setedsAdd = true )
	{
		$this->test2 = 1;
		foreach ( static::$_fields as $name ) {
			if( !empty( $data[ $name ] )) {
				if( $setedsAdd ){
					$this->_setedFields[ $name ] = true;
				}
				$this->setFieldData( $name, $data[ $name ] );
			}
		}
	}


	public function setFieldData( $name, $value )
	{
		if( array_search( $name, static::$_fields) !== false ){
			if( isset( static::$_convertLoad[ $name ] ) ){
				$convertMethod = static::$_convertLoad[ $name ];
				$value = $this->$convertMethod( $value );
			}

			$this->_fieldsData[ $name ] = $value;
		}
	}


	public function getFieldsData()
	{
		$data = [];
		foreach ( static::$_fields as $name ) {
			if( !empty( $this->_fieldsData[ $name ] )) {
				$data[ $name ] = $this->getFieldData( $name );
			}
		}
		return $data;
	}


	public function getFieldData( $name )
	{
		if( isset( $this->_fieldsData[ $name ] )) {
			$value = $this->_fieldsData[ $name ];
			if( isset( static::$_convertSave[ $name ] ) ){
				$convertMethod = static::$_convertSave[ $name ];
				$value = static::$convertMethod( $value );
			}
			return $value;
		} else {
			return null;
		}
	}


	public function getSetedFieldsData()
	{
		$data = [];
		foreach( $this->_setedFields as $name => $value ) {
			$data[ $name ] = $this->getFieldData( $name );
		}
		return $data;
	}


	public function dataUnserialise( $value )
	{
		return unserialize( $value );
	}

	public function dataSerialize( $value )
	{
		return serialize( $value );
	}


}
