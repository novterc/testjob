<?php

return [

	'public' => [
		'url' => 'http://serv.tiranka.ru:10001',
		'prefix' => '',
		'prefixAdmin' => '/ap',
		'userImage' => '/user_img',
	],

	'template' => [
		'views' => '/views',
		'cache' => '/tmp/bladeCache',
	],

	'paths' => [
		'tmpUploadFile' => '/tmp/uploadFile',
		'userImage' => '/public/user_img',
	],

	'userImg' => [
		'maxSize' => [ 'x' => 320, 'y' => 240 ],
		'quality' => 100,
		'accessTypes' => [
			'image/jpeg',
			'image/png',
			'image/gif',
		],
	],

];
